﻿using PromoTester1.UserControlFolder;
using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PromoTester1.WebCamera
{
    /// <summary>
    /// Interaction logic for WebcamCheckDialog.xaml
    /// </summary>
    public partial class WebcamCheckDialog : Window
    {
        #region Fields
        private String fpsMessage;
        public String FpsMessage
        {
            get { lock (this) { return fpsMessage; } }
            set { lock (this) { fpsMessage = value; } }
        }

        private bool videoStarted;
        private bool IsVideoStarted
        {
            get { lock (this) { return videoStarted; } }
            set { lock (this) { videoStarted = value; } }
        }

        private WebCamReader webCamReader;
        private WebCamReader webCamReaderSync
        {
            get { lock (this) { return webCamReader; } }
        }

        private bool allGreen;
        public bool IsAllGreen
        {
            get { lock (this) { return allGreen; } }
        }

        private int recVideoFps;
        public int RecVideoFps
        {
            get { lock (this) { return recVideoFps; } }
        }

        private SynchronizationContext synchronizationContext;
        int error1 = 0;
        int error2 = 0;
        int error3 = 0;
        int error4 = 0;
        int error5 = 0;
        int error6 = 0;
        #endregion

        #region Contructors and Window_Load
        public WebcamCheckDialog(WebCamReader camReader)
        {
            InitializeComponent();
            synchronizationContext = SynchronizationContext.Current;
            webCamReader = camReader;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Thread webCamVideoThread = new Thread(new ThreadStart(webCamPlay));
            webCamVideoThread.IsBackground = true;

            webCamVideoThread.Start();

        }
        #endregion

        #region UI methods

        private void btnZoomOut_Click(object sender, RoutedEventArgs e)
        {
            webCamReaderSync.zoomOut();
        }

        private void btnZoomIn_Click(object sender, RoutedEventArgs e)
        {
            webCamReaderSync.zoomIn();
        }

        private void btnContinue_Click(object sender, RoutedEventArgs e)
        {
            IsVideoStarted = false;
            btnContinue.IsEnabled = false;
            btnContinue.Content = PromoTester1.Properties.Resources.webCamCheckDialog_closingButtonText;

            String warningsLog = $"{WebTaskMediaDialog.localResultsPath}" + "\\" + PromoTester1.Properties.Settings.Default.warningsLogFileName;
            String webCamShotPath = $"{WebTaskMediaDialog.localResultsPath}" + "\\webcamShot.jpg";

            //if there is bad FACE ILLUMINATION (Red color)
            if (error4 == 1)
            {
                File.AppendAllText(warningsLog, Helper.GetUnixTimestamp() + " WebCam Check Dialog bad settings: FACE ILLUMINATION, webcam shot path: " + Environment.NewLine);
            }

            //if there is bad BACKLIGHT (Red color)
            if (error5 == 1)
            {
                File.AppendAllText(warningsLog, Helper.GetUnixTimestamp() + " WebCam Check Dialog bad settings: BACKLIGHT " + Environment.NewLine);
            }

            //if there is bad CAMERA_NOISE (Red color)
            if (error6 == 1)
            {
                File.AppendAllText(warningsLog, Helper.GetUnixTimestamp() + " WebCam Check Dialog bad settings: CAMERA NOISE " + Environment.NewLine);
            }

            //if there is bad FACE_SIZE_AND_POSITION (Red color)
            if ((error1 == 1) || (error2 == 1) || (error3 == 1))
            {
                File.AppendAllText(warningsLog, Helper.GetUnixTimestamp() + " WebCam Check Dialog bad settings: FACE SIZE AND POSITION " + Environment.NewLine);
            }
        }
        #endregion

        #region Main Method
        private void webCamPlay()
        {
            try
            {
                TesterAlgo.InitializeTestFace($"{WebTaskMediaDialog.repositoryPath}", 640, 480, "data//", PromoTester1.Properties.Settings.Default.image_min_quality_live);

                 synchronizationContext.Send(new SendOrPostCallback(
                            delegate (object state)
                            {
                                this.btnContinue.IsEnabled = true;
                            }
                            ), null);

                IsVideoStarted = true;
                Bitmap bmp = null;
                BitmapImage bmpImg = null;



                int frameCounter = 0;
                int start = DateTime.Now.Hour * 3600000 + DateTime.Now.Minute * 60000 + DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;

                while (IsVideoStarted)
                {
                    frameCounter++;
                    webCamReaderSync.nextImage();

                    IntPtr webCamImagePtr = webCamReaderSync.CurrentImagePtr;

                   
                    try
                    {
                        //Displays red and green rect inside camera image that displays is you positioned well. For this method to work it requires (data, opencvCamera, opencvTemp) to be in debug folder
                        int errCode = TesterAlgo.getFaceRectQuality(webCamImagePtr, ref error1, ref error2, ref error3, ref error4, ref error5, ref error6);//, "data//");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("EXEPTION in WebcamCheckDialog line 158 !!! and error message is :" + ex.Message);
                    }

                    bmp = webCamReaderSync.getWebCamShot();
                    allGreen = (error1 == 0) && (error2 == 0) && (error3 == 0) && (error4 == 0) && (error5 == 0) && (error6 == 0);

                    if (bmp != null)
                    {
                        synchronizationContext.Send(new SendOrPostCallback(
                            (a) =>
                            {
                                //bmp.RotateFlip(RotateFlipType.Rotate180FlipX);

                                //for WPF image has to be in BitmapImage format to be displayed in <image> elemenent
                                bmpImg = BitmapHelpers.ToBitmapImage(bmp);
                                webCamPictureBox.Source = bmpImg;

                                if (error4 == 1)
                                {
                                    panelFACE_ILLUMINATION.Background = new SolidColorBrush(Colors.Red);
                                }
                                else
                                {
                                    panelFACE_ILLUMINATION.Background = new SolidColorBrush(Colors.LightGreen);
                                }

                                if (error5 == 1)
                                {
                                    panelBACKLIGHT.Background = new SolidColorBrush(Colors.Red);
                                }
                                else
                                {
                                    panelBACKLIGHT.Background = new SolidColorBrush(Colors.LightGreen);
                                }

                                if (error6 == 1)
                                {
                                    panelCAMERA_NOISE.Background = new SolidColorBrush(Colors.Red);
                                }
                                else
                                {
                                    panelCAMERA_NOISE.Background = new SolidColorBrush(Colors.LightGreen);
                                }

                                if ((error1 == 0) && (error2 == 0) && (error3 == 0))
                                {
                                    panelFACE_SIZE_AND_POSITION.Background = new SolidColorBrush(Colors.LightGreen);
                                }
                                else
                                {
                                    panelFACE_SIZE_AND_POSITION.Background = new SolidColorBrush(Colors.Red);
                                }
                            }
                        ), null);
                    }
                }
                int stop = DateTime.Now.Hour * 3600000 + DateTime.Now.Minute * 60000 + DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;
                //calculating FPS
                recVideoFps = (int)(frameCounter * 1000 / (stop - start));
                FpsMessage += Environment.NewLine + "WebCamCheck FPS = " + recVideoFps.ToString();
            }

            catch (ThreadAbortException)
            {
                //Ignored if thread is aborted
            }

            catch (WebCamReaderException webCamEx)
            {
                if (webCamEx.MessageType == WebCamReaderException.WARNING_MESSAGE)
                {
                    MessageBox.Show(webCamEx.Message, PromoTester1.Properties.Resources.warning_caption, MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {

                    MessageBox.Show(webCamEx.Message, PromoTester1.Properties.Resources.error_caption, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, PromoTester1.Properties.Resources.error_caption, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            finally
            {
                try
                {
                    //Thread.Sleep(50);    
                    TesterAlgo.CleanupLiveStream();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, PromoTester1.Properties.Resources.error_caption, MessageBoxButton.OK, MessageBoxImage.Error);
                }

                synchronizationContext.Send(new SendOrPostCallback(
                        delegate (object state)
                        {
                            this.Close();
                        }
                    ), null);
            }
        }
        #endregion
    }
}
