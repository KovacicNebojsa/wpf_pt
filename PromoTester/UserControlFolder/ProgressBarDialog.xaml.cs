﻿using System;
using System.Threading.Tasks;
using System.Windows;

namespace PromoTester1.UserControlFolder
{
    /// <summary>
    /// Interaction logic for ProgressBarDialog.xaml
    /// </summary>
    public partial class ProgressBarDialog : Window
    {
        public Action Worker { get; set; }
        public ProgressBarDialog(Action worker)
        {
            InitializeComponent();
            if (worker == null)
            {
                throw new ArgumentException();
            }
            Worker = worker;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(Worker).ContinueWith(t => { this.Close();
                MessageBox.Show("Files Uploaded");
            }, TaskScheduler.FromCurrentSynchronizationContext());
           
        }
    }
}
