﻿using CefSharp;
using CefSharp.Wpf;
using PromoTester1.WebCamera;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

using System.Xml;

namespace PromoTester1.UserControlFolder
{
    //DeleteAfterTest
    //public class DotNetMessage
    //{
    //    public void Show(string message)
    //    {
    //        MessageBox.Show(message);
    //    }
    //}
    public partial class WebTaskMediaDialog : UserControl
    {
        #region Fields
        SynchronizationContext synchronizationContext = null;
        XmlDocument xmlDoc = null;
        public static List<XmlElement> allElementsInDefinition = null;

        public static List<XmlElement> infoElemets = null;
        public static List<XmlElement> calibrationElements = null;
        public static List<XmlElement> testCalibrationElements = null;
        public static List<XmlElement> videotaskElements = null;
        public static List<XmlElement> watchtaskElements = null;
        public static List<XmlElement> clicktaskElements = null;
        public static List<XmlElement> webtaskElements = null;

        public static string repositoryPath = Directory.GetCurrentDirectory();
        public static string localPath = $"{repositoryPath}\\PROMOTESTER_REPOSITORY\\71751909\\x_protech_proba_5491\\";
        public static string localResultsPath = $"{repositoryPath}\\PROMOTESTER_REPOSITORY\\71751909\\x_protech_proba_5491\\rec\\";
        public static string generatedPagesPath = $"{repositoryPath}\\PROMOTESTER_REPOSITORY\\71751909\\x_protech_proba_5491\\test_generated_pages\\";
        public string index = "index";
        private bool noWebCam = false;
        WebCamReader webCamReader;
        public bool clicked = false;
        Thread webCamCheckThread ;
        public static int i = 0;
        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            chromiumBrowser.FrameLoadStart += ChromiumBrowser_FrameLoadStart;

        }
        #region Constructor/s
        public WebTaskMediaDialog()
        {
            InitializeComponent();
            infoElemets = new List<XmlElement>();
            calibrationElements = new List<XmlElement>();
            testCalibrationElements = new List<XmlElement>();
            watchtaskElements = new List<XmlElement>();
            clicktaskElements = new List<XmlElement>();
            videotaskElements = new List<XmlElement>();
            webtaskElements = new List<XmlElement>(); 
            chromiumBrowser.Address = $"{generatedPagesPath}info--intro01-en.html";
            synchronizationContext = SynchronizationContext.Current;
            xmlDoc = new XmlDocument();
            xmlDoc.Load($"{localPath}definition.xml");
            Directory.CreateDirectory($"{localPath}test_generated_pages");
            allElementsInDefinition = Helper.ReadAllXmlElements(xmlDoc.DocumentElement);
            Helper.SeperateElements(allElementsInDefinition, ref infoElemets, ref calibrationElements, ref testCalibrationElements, ref watchtaskElements, ref videotaskElements, ref clicktaskElements, ref webtaskElements);
            GenerateHTMLs.GenerateHtmlForAllElements();
            var x = clicktaskElements[0].ChildNodes;
            

            //DeleteAfterTest
            //https://www.youtube.com/watch?v=T10Xfe4bepo&ab_channel=EmbeddedBrowsers
            //chromiumBrowser.RegisterJsObject("dotNetMessage", new DotNetMessage());
            //chromiumBrowser.IsBrowserInitializedChanged += (sender, args) =>
            //{
            //    if (chromiumBrowser.IsBrowserInitialized)
            //    {
            //        chromiumBrowser.LoadHtml(File.ReadAllText("index.html"));
            //    }
            //};

            #region Camera
            if (File.Exists("nowebcam.pt"))
            {
                String nowebcam = File.ReadAllText("nowebcam.pt").Trim();
                if (nowebcam.CompareTo("true") == 0)
                {
                    noWebCam = true;
                }
                else
                {
                    noWebCam = false;
                }
            }
            else
            {
                noWebCam = false;
            }
            if (noWebCam)
            {
                webCamReader = new WebCamReader();
            }
            else
            {
                webCamReader = new WebCamReader(PromoTester1.Properties.Settings.Default.webCamId);
            }
            #endregion
        }
        

        #endregion
        private void ChromiumBrowser_FrameLoadStart(object sender, FrameLoadStartEventArgs e)
        {
            if (e.Frame.IsMain)
            {
                XmlElement firstChild = null;
                var outputofFirstChild = "";
                //showing up camera on info element before first calibration
                if (e.Url.Contains("intro08"))
                {
                    synchronizationContext.Send((a) =>
                    {
                        try
                        {
                            String video_input_list;
                            StringBuilder tmp_video_input_list = new StringBuilder(1000);
                            OpenCVCamera.FindVideoInputandList(tmp_video_input_list);

                            video_input_list = tmp_video_input_list.ToString();
                            String[] webcams = video_input_list.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                            if (webcams.Length > 0)
                            {
                                WebCamSelectDialog wcsd = new WebCamSelectDialog(webcams);
                                bool? result = wcsd.ShowDialog();
                                if (result == true)
                                {
                                    PromoTester1.Properties.Settings.Default.webCamId = wcsd.SelectedWebcamId;
                                    PromoTester1.Properties.Settings.Default.Save();
                                    webCamReader = new WebCamReader(PromoTester1.Properties.Settings.Default.webCamId);
                                    webCamCheckThread = new Thread(new ThreadStart(webCamReader.camCheckForever));
                                    webCamCheckThread.IsBackground = true;
                                    webCamCheckThread.Start();
                                    WebcamCheckDialog wb = new WebcamCheckDialog(webCamReader);
                                    wb.ShowDialog();

                                }
                            }
                            else
                            {
                                MessageBox.Show(PromoTester1.Properties.Resources.noWebcamError, PromoTester1.Properties.Resources.error_caption, MessageBoxButton.OK, MessageBoxImage.Error);
                                File.AppendAllText(PromoTester1.Properties.Settings.Default.errorsLogFileName, Helper.GetUnixTimestamp() + " " + PromoTester1.Properties.Resources.noWebcamError + Environment.NewLine);
                            }
                        }
                        catch
                        {
                            MessageBox.Show(PromoTester1.Properties.Resources.noWebcamError, PromoTester1.Properties.Resources.error_caption, MessageBoxButton.OK, MessageBoxImage.Error);
                            File.AppendAllText(PromoTester1.Properties.Settings.Default.errorsLogFileName, Helper.GetUnixTimestamp() + " " + PromoTester1.Properties.Resources.noWebcamError + Environment.NewLine);
                        }

                    }, null);
                   

                }

                //if statment before calibration because calibrationImage string is also contained in testcalibrationImage
                if (e.Url.Contains("testcalibration"))
                {
                    firstChild = testCalibrationElements[0].FirstChild as XmlElement;
                    outputofFirstChild = firstChild.GetAttribute("output");
                    if (e.Url.Contains(outputofFirstChild) && e.Url.Contains(testCalibrationElements[0].Name))
                    {
                        //and test calibration is always index of 5 after calibration
                        i = 5;
                        var imagesThread = new Thread(() =>
                        {
                            PlayImagesChromium(testCalibrationElements);
                        });
                        imagesThread.IsBackground = true;
                        imagesThread.Start();
                        //setting up everything for next element
                        i++;
                    }
                }
                else if (e.Url.Contains("calibration"))
                {
                    firstChild = calibrationElements[0].FirstChild as XmlElement;
                    outputofFirstChild = firstChild.GetAttribute("output");
                    if (e.Url.Contains(outputofFirstChild) && e.Url.Contains(calibrationElements[0].Name))
                    {
                        //calibration is always index of 4
                        i = 4;
                        var imagesThread = new Thread(() =>
                        {
                            PlayImagesChromium(calibrationElements);
                        });
                        imagesThread.IsBackground = true;
                        imagesThread.Start();
                        //setting up everything for next element
                        i++;
                    }

                }
                else if (e.Url.Contains("watchtask"))
                {
                    firstChild = watchtaskElements[0].FirstChild as XmlElement;
                    outputofFirstChild = firstChild.GetAttribute("output");
                    if (e.Url.Contains(outputofFirstChild) && e.Url.Contains(watchtaskElements[0].Name))
                    {
                        WatchTask watchTask = new WatchTask(chromiumBrowser, watchtaskElements, synchronizationContext);
                        watchTask.PlayImageThread();
                    }
                    else
                    {
                        //upisivanje u foldere od analize.
                        //dodati logiku u watchTask klasi
                    }
                }
                else if (e.Url.Contains("clicktask"))
                {
                    firstChild = clicktaskElements[0].FirstChild as XmlElement;
                    outputofFirstChild = firstChild.GetAttribute("output");
                    if (e.Url.Contains(outputofFirstChild) && e.Url.Contains(clicktaskElements[0].Name))
                    {
                        ClickTask clickTask = new ClickTask(chromiumBrowser, clicktaskElements, synchronizationContext);
                        clickTask.PlayImageThread();
                    }
                    else
                    {
                        //upisivanje u foldere od analize.
                        //dodati logiku u watchTask klasi
                    }
                }


            }

        }

        #region HelperMethods

        public void PlayImagesChromium(List<XmlElement> elementsToDisplay)//ne moze u helper clasu zbog UI elemenata
        {
            int timeout = 0;
            string fullNameOfElement;
            string elementName = "";
            string elementType = "";
            string mainElementOutput = "";
            for (int j = 0; j < elementsToDisplay.Count; j++)
            {
                //skip first element of every list because index = 0 is saved for info element
                if (elementsToDisplay[j].HasChildNodes )
                {
                    continue;
                }

                //j == 1 -  skip first child Element because it is required to be loaded to call PlayImagesChormium method, but letting him do his time
                if (j == 1)
                {
                    timeout = (int.Parse(elementsToDisplay[j].GetAttribute("timeout"))) * 1000;
                    Thread.Sleep(timeout);
                }

                else
                {
                    timeout = (int.Parse(elementsToDisplay[j].GetAttribute("timeout"))) * 1000;
                    fullNameOfElement = elementsToDisplay[j].GetAttribute("src");
                    
                    var result = fullNameOfElement.Split('/');
                    var folderName = result[0];
                    if (elementsToDisplay[0].Name == "testcalibration")
                    {
                        folderName = "testcalibrationImages";
                    }
                    elementType = elementsToDisplay[0].Name;
                    mainElementOutput = elementsToDisplay[0].GetAttribute("output");
                    elementName = elementsToDisplay[j].GetAttribute("output");
                    synchronizationContext.Send((a) =>
                    {
                        chromiumBrowser.Address = $"file:///{generatedPagesPath}{elementType}-{mainElementOutput}-{elementName}.html";
                    }, null);
                    Thread.Sleep(timeout);
                }
            }
            synchronizationContext.Send((a) =>
            {
                var nextElementInOrder = GetNextXmlElement(allElementsInDefinition, ref i);
                DisplayToChromium( nextElementInOrder);

            }, null);
        }
        public void DisplayToChromium( XmlElement elementToDisplay)
        {
            var elementType = elementToDisplay.Name;
            var mainElementOutput = elementToDisplay.GetAttribute("output");
            var elementName = elementToDisplay.GetAttribute("info");

            var result = elementName.Split('/','.');
            if (result.Length > 1)
            {
                elementName = result[1];
            }
            chromiumBrowser.Address = $"file:///{generatedPagesPath}{elementType}-{mainElementOutput}-{elementName}.html";
        }

        public string GetNextXmlElementName(List<XmlElement> allElementsForOneTask, ref int currentXmlElementIndex)
        {
            var elementName = "";
            if (allElementsForOneTask.Count > currentXmlElementIndex && currentXmlElementIndex >= 0)
            {
                if (allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("info") == 0)
                {
                    elementName = Helper.TakeOnlyElementName(allElementsForOneTask[currentXmlElementIndex], "src");

                    return "info_" + elementName;
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("calibration") == 0))
                {
                    elementName = Helper.TakeOnlyElementName(allElementsForOneTask[currentXmlElementIndex], "info");

                    return "calibration_" + elementName;
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("testcalibration") == 0))
                {
                    elementName = Helper.TakeOnlyElementName(allElementsForOneTask[currentXmlElementIndex], "info");

                    return "testcalibration_" + elementName;
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("watchtask") == 0))
                {
                    elementName = Helper.TakeOnlyElementName(allElementsForOneTask[currentXmlElementIndex], "info");
                    return "watchtaskInfo_" + elementName;
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("clicktask") == 0))
                {
                    elementName = Helper.TakeOnlyElementName(allElementsForOneTask[currentXmlElementIndex], "info");
                    return "clicktaskInfo_" + elementName;
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("webtask") == 0))
                {
                    elementName = Helper.TakeOnlyElementName(allElementsForOneTask[currentXmlElementIndex], "info");
                    return "webtaskInfo_" + elementName;
                }
                else
                {
                    return "";
                }
            }
            //last child element (finish test)
            else
            {
                return "done";
            }
        }
        public static XmlElement GetNextXmlElement(List<XmlElement> allElementsForOneTask, ref int currentXmlElementIndex)
        {

            if (allElementsForOneTask.Count > currentXmlElementIndex && currentXmlElementIndex >= 0)
            {
                if (allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("info") == 0)
                {
                    return allElementsForOneTask[currentXmlElementIndex];
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("calibration") == 0))
                {
                    return allElementsForOneTask[currentXmlElementIndex];
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("testcalibration") == 0))
                {
                    return allElementsForOneTask[currentXmlElementIndex];
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("watchtask") == 0))
                {
                    return allElementsForOneTask[currentXmlElementIndex];
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("clicktask") == 0))
                {
                    return allElementsForOneTask[currentXmlElementIndex];
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("videotask") == 0))
                {
                    return allElementsForOneTask[currentXmlElementIndex];
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("webtask") == 0))
                {

                    return allElementsForOneTask[currentXmlElementIndex];
                }
                else
                {
                    return allElementsForOneTask[currentXmlElementIndex];
                }
            }
            else
            {
                return null;
            }
        }

        #endregion





    }
}
