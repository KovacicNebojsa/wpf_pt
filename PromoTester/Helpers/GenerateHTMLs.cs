﻿using PromoTester1.UserControlFolder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace PromoTester1
{
    public static class GenerateHTMLs
    {

        #region All-Info-Slides
        //still waiting for logic for video and webtask
        public static void GenerateHtmlForAllInfoElements(List<XmlElement> allElementsInDefinition)
        {
            for (int i = 0; i < allElementsInDefinition.Count; i++)
            {
                string nextElementLink = "", prevElementLink = "", nextElementName = "", prevElementName = "", infoElementName = "";
                XmlElement mainElement = null, nextElement = null, prevElement = null;

                int nextIndex = i + 1;
                int prevIndex = i - 1;

                if ((allElementsInDefinition[i].Name.Trim().CompareTo("info") == 0))
                {
                    mainElement = allElementsInDefinition[i];
                    if (nextIndex < allElementsInDefinition.Count)
                    {
                        nextElement = allElementsInDefinition[nextIndex];
                    }
                    if (allElementsInDefinition[0] != allElementsInDefinition[i])
                    {
                        prevElement = allElementsInDefinition[prevIndex];
                    }

                    CreateHtmlUsingInfoTemplateNew(WebTaskMediaDialog.generatedPagesPath, mainElement, prevElement, nextElement);
                }
                else if (allElementsInDefinition[i].HasAttribute("info") && (allElementsInDefinition[i].HasChildNodes))
                {

                    mainElement = allElementsInDefinition[i];
                    if (nextIndex < allElementsInDefinition.Count)
                    {
                        nextElement = allElementsInDefinition[i].FirstChild as XmlElement;
                    }
                    if (allElementsInDefinition[0] != allElementsInDefinition[i])
                    {
                        prevElement = allElementsInDefinition[prevIndex];
                    }

                    CreateHtmlUsingInfoTemplateNew(WebTaskMediaDialog.generatedPagesPath, mainElement, prevElement, nextElement);

                }
               
            }

        }
        public static void CreateHtmlUsingInfoTemplateNew(string path, XmlElement mainElement, XmlElement prevElement, XmlElement nextElement)
        {
            var mainElementType = mainElement.Name;
            var mainOutput = mainElement.GetAttribute("output");
            var imageName = "";
            var imagePath = "";
            imagePath = mainElement.GetAttribute("src");
            if (imagePath == "")
            {
                imagePath = mainElement.GetAttribute("info");
            }
            var result = imagePath.Split('/', '.');
            if (result.Length > 1)
            {
                imageName = result[1];
                //imageName = imageName.Replace(".jpg", "");
            }

            string nextElementLink = "", prevElementLink = "", nextElementOutput = "", prevElementOutput = "", nextImgName = "", prevImgName = "", nextElementType = "", prevElementType = "";

            if (nextElement != null)
            {
                nextImgName = nextElement.GetAttribute("src");
                if (nextImgName == "")
                {
                    nextElementOutput = nextElement.GetAttribute("output");
                    nextImgName = nextElement.GetAttribute("info");
                }

                //nextImgName = nextImgName.Replace("infoImages/", "");
                //nextImgName = nextImgName.Replace(".jpg", "");
                var resultNext = nextImgName.Split('/', '.');
                nextElementType = nextElement.Name;

                if (resultNext.Length > 1)
                {
                    nextImgName = resultNext[1];
                }
                nextElementLink = $"file:///{WebTaskMediaDialog.generatedPagesPath}{nextElementType}-{nextElementOutput}-{nextImgName}.html";

                if (mainElement.Name != "info" && mainElement.HasChildNodes)
                {
                    nextElement = nextElement as XmlElement;
                    mainOutput = mainElement.GetAttribute("output");
                    nextElementOutput = nextElement.GetAttribute("output");

                    nextElementLink = $"file:///{WebTaskMediaDialog.generatedPagesPath}{mainElementType}-{mainOutput}-{nextElementOutput}.html";
                }
            }
            if (prevElement != null)
            {

                prevImgName = prevElement.GetAttribute("src");
                if (prevImgName == "")
                {
                    prevElementOutput = prevElement.GetAttribute("output");
                    prevImgName = prevElement.GetAttribute("info");
                }

                //prevImgName = prevImgName.Replace("infoImages/", "");
                //prevImgName = prevImgName.Replace(".jpg", "");
                var resultPrev = prevElementOutput.Split('/', '.');
                prevElementType = prevElement.Name;
                if (resultPrev.Length > 1)
                {
                    prevImgName = resultPrev[1];
                }
                prevElementLink = $"file:///{WebTaskMediaDialog.generatedPagesPath}{prevElementType}-{prevElementOutput}-{prevImgName}.html";
            }


            if (File.Exists($"{path}{mainElementType}-{mainOutput}-{imageName}.html"))
            {
                File.Delete($"{path}{mainElementType}-{mainOutput}-{imageName}.html");
            }
            string htmlText = File.ReadAllText(@"HtmlTemplates\InfoTemplate\infoSlideTemplate.html");


            htmlText = htmlText.Replace("[imgName]", imagePath);
            htmlText = htmlText.Replace("[backBtnLink]", prevElementLink);
            htmlText = htmlText.Replace("[nextBtnLink]", nextElementLink);
            File.AppendAllText($"{path}{mainElementType}-{mainOutput}-{imageName}.html", htmlText);
        }


        #endregion

        #region Calibration and TestCalibration
        public static void GenerateHtmlUsingCalibrationTemplate(List<XmlElement> calibrationList)
        {
            string path = WebTaskMediaDialog.generatedPagesPath;
            int calibGridDimensionX = 0;
            int calibGridDimensionY = 0;
            string childElementOutput = "";
            var mainElementOutput = "";
            string color = "blue";
            string calibrationType = "testcalibration";

            if (calibrationList[0].Name == "calibration")
            {
                color = "red";
                calibrationType = "calibration";
            }
            for (int i = 0; i < calibrationList.Count; i++)
            {

                if (i == 0)
                {
                    calibGridDimensionX = int.Parse(calibrationList[i].GetAttribute("gridW"));
                    calibGridDimensionY = int.Parse(calibrationList[i].GetAttribute("gridH"));
                    mainElementOutput = calibrationList[i].GetAttribute("output");

                    //skip because first element is info element (skip in that case)
                    continue;
                }
                childElementOutput = calibrationList[i].GetAttribute("output");
                int calibPointX = int.Parse(calibrationList[i].GetAttribute("calibPointX"));
                int calibPointY = int.Parse(calibrationList[i].GetAttribute("calibPointY"));

                if (File.Exists($"{path}{calibrationType}-{mainElementOutput}-{childElementOutput}.html"))
                {
                    File.Delete($"{path}{calibrationType}-{mainElementOutput}-{childElementOutput}.html");
                }

                string htmlText = File.ReadAllText(@"HtmlTemplates\CalibrationTemplate\calibrationTemplate.html");

                htmlText = htmlText.Replace("[calibPointXTemplate]", calibPointX.ToString());
                htmlText = htmlText.Replace("[calibGridDimensionXTemplate]", calibGridDimensionX.ToString());

                htmlText = htmlText.Replace("[calibPointYTemplate]", calibPointY.ToString());
                htmlText = htmlText.Replace("[calibGridDimensionYTemplate]", calibGridDimensionY.ToString());

                htmlText = htmlText.Replace("[colorTemplate]", color);

                File.AppendAllText($"{path}{calibrationType}-{mainElementOutput}-{childElementOutput}.html", htmlText);
            }



        }
        #endregion

        #region WatchTask & ClickTask
        public static void GenerateHtmlForWatchScreenElements(List<XmlElement> watchScreenElements)
        {
            XmlElement firstElementOfNewTask = null;
            for (int i = 0; i < watchScreenElements.Count; i++)
            {
                //if more then 1 watchtask to take first element
                if (watchScreenElements[i].Name.Contains("watchtask"))
                {
                    firstElementOfNewTask = watchScreenElements[i];
                }

                if (watchScreenElements[i].Name.Contains("watchscreen"))
                {
                    CreateHtmlUsingFullScreenTemplate(WebTaskMediaDialog.generatedPagesPath, firstElementOfNewTask, watchScreenElements[i]);
                }

               
            }

        }
        public static void GenerateHtmlForClickScreenElements(List<XmlElement> clicktaskElements)
        {
            XmlElement firstElementOfNewTask = null;
            for (int i = 0; i < clicktaskElements.Count; i++)
            {
                //if more then 1 clicktask to take first element
                if (clicktaskElements[i].Name.Contains("clicktask"))
                {
                    firstElementOfNewTask = clicktaskElements[i];
                }
                if (clicktaskElements[i].Name.Contains("clickscreen"))
                {
                     CreateHtmlUsingFullScreenTemplate(WebTaskMediaDialog.generatedPagesPath, firstElementOfNewTask, clicktaskElements[i]);
                }
            }

        }
        public static void CreateHtmlUsingFullScreenTemplate(string path, XmlElement parentElement, XmlElement element)
        {
            var parentElementType = parentElement.Name;
            var parentOutput = parentElement.GetAttribute("output");

            var elementName = element.GetAttribute("src");
            var elementOutput = element.GetAttribute("output");


            if (File.Exists($"{path}{parentElementType}-{parentOutput}-{elementOutput}.html"))
            {
                File.Delete($"{path}{parentElementType}-{parentOutput}-{elementOutput}.html");
            }
            string htmlText = File.ReadAllText(@"HtmlTemplates\FullScreenTemplate\FullScreenTemplate.html");

            htmlText = htmlText.Replace("[imgName]", elementName);
            File.AppendAllText($"{path}{parentElementType}-{parentOutput}-{elementOutput}.html", htmlText);
        }
        #endregion

        #region Main method for generating
        //MAIN  DOVRSITI SA VIDEO TASKOM i web taskom
        public static void GenerateHtmlForAllElements()
        {
            GenerateHtmlUsingCalibrationTemplate(WebTaskMediaDialog.calibrationElements);
            GenerateHtmlUsingCalibrationTemplate(WebTaskMediaDialog.testCalibrationElements);
            GenerateHtmlForWatchScreenElements(WebTaskMediaDialog.watchtaskElements);
            GenerateHtmlForClickScreenElements(WebTaskMediaDialog.clicktaskElements);
            GenerateHtmlForAllInfoElements(WebTaskMediaDialog.allElementsInDefinition);

        }

        #endregion
    }
}
