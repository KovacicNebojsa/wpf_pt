﻿
using PromoTester1.UserControlFolder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;

namespace PromoTester1
{
    public static class Helper
    {
        public static List<XmlElement> ReadAllXmlElements(XmlNode element)
        {
            List<XmlElement> xmlElements = new List<XmlElement>();

            if (element.HasChildNodes)
            {
                for (int i = 0; i < element.ChildNodes.Count; i++)
                {
                    XmlNode node = element.ChildNodes[i];

                    if (node.NodeType == XmlNodeType.Element)
                    {
                        xmlElements.Add(node as XmlElement);
                    }
                }
            }
            return xmlElements;
        }
        public static string TakeOnlyElementName(XmlElement element, string attribute)
        {
            string infoElementName = element.GetAttribute($"{attribute}");
            infoElementName = infoElementName.Replace('/', '_');
            infoElementName = infoElementName.Replace(".jpg", "");
            return infoElementName;
        }

        /// <summary>
        /// Filling up all list of definition tasks.
        /// Excluding infoElements and allelementsIndefinitions, the elements in these methods are separated from the index based on 0 which is preserved for the main elements that contain most important information about element
        /// Child nodes of elements starts from index 1
        /// </summary>
        /// <param name="allElementsInDefinition"> All elements in definition xml file </param>
        /// <param name="infoElemets"> Has no child nodes </param>
        /// <param name="calibrationElements"> List that contains main (info element), containts images with calibration points  </param>
        /// <param name="testCalibrationElements"> List that contains main (info element), containts images with testcalibration points </param>
        /// <param name="watchtaskElements"> List that contains main (info element) and watchscreens(images to display) </param>
        /// <param name="clicktaskElements">List that contains main (info element) and clickscreen child node elements</param>
        /// <param name="webtaskElements">List that contains main (info element) and webscreen child node elements</param>
        internal static void SeperateElements(List<XmlElement> allElementsInDefinition, ref List<XmlElement> infoElemets, ref List<XmlElement> calibrationElements, ref List<XmlElement> testCalibrationElements, ref List<XmlElement> watchtaskElements,  ref List<XmlElement> videotaskElements, ref List<XmlElement> clicktaskElements, ref List<XmlElement> webtaskElements)
        {
            foreach (var element in allElementsInDefinition)
            {
                if ((element.Name.Trim().CompareTo("info") == 0) && !string.IsNullOrWhiteSpace(element.GetAttribute("src")))
                {
                    infoElemets.Add(element);
                }
                if ((element.Name.Trim().CompareTo("calibration") == 0))
                {
                    //adding main task of element which contains important information about element settings
                    calibrationElements.Add(element);

                    //browsing through all elements of one main element and adding it in list starting from index 1
                    if (element.HasChildNodes)
                    {
                        calibrationElements.AddRange(ReadAllXmlElements(element));
                    }
                }
                if ((element.Name.Trim().CompareTo("testcalibration") == 0))
                {
                    testCalibrationElements.Add(element);
                    if (element.HasChildNodes)
                    {
                        testCalibrationElements.AddRange(ReadAllXmlElements(element));
                    }
                }
                if ((element.Name.Trim().CompareTo("watchtask") == 0))
                {
                    watchtaskElements.Add(element);
                    if (element.HasChildNodes)
                    {
                        watchtaskElements.AddRange(ReadAllXmlElements(element));
                    }
                }
                if ((element.Name.Trim().CompareTo("videotask") == 0))
                {
                    videotaskElements.Add(element);
                    if (element.HasChildNodes)
                    {
                        videotaskElements.AddRange(ReadAllXmlElements(element));
                    }
                }
                if ((element.Name.Trim().CompareTo("clicktask") == 0))
                {
                    clicktaskElements.Add(element);
                    if (element.HasChildNodes)
                    {
                        clicktaskElements.AddRange(ReadAllXmlElements(element));
                    }
                }
                if ((element.Name.Trim().CompareTo("webtask") == 0))
                {
                    webtaskElements.Add(element);
                }
            }
        }

        public static double GetUnixTimestamp()
        {
            return (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        }

        public static double GetUnixTimestamp(DateTime date)
        {
            return (date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        }

    }
}

