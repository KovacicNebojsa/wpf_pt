﻿using CefSharp.Wpf;
using PromoTester1.UserControlFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace PromoTester1
{
    public class WatchTask
    {
        ChromiumWebBrowser chromiumBrowser;
        List<XmlElement> watchtaskElements;
        List<XmlElement> allElementsInDefinition;
        SynchronizationContext synchronizationContext;
        static int i;

        public WatchTask(ChromiumWebBrowser chromiumBrowser, List<XmlElement> watchtaskElements, SynchronizationContext synchronizationContext)
        {
            this.chromiumBrowser = chromiumBrowser;
            this.watchtaskElements = watchtaskElements;
            this.synchronizationContext = synchronizationContext;
            i = WebTaskMediaDialog.i;
            allElementsInDefinition = WebTaskMediaDialog.allElementsInDefinition;
        }

        public  void PlayImageThread()
        {
            var imagesThread = new Thread(() =>
            {
                PlayImagesChromium(watchtaskElements);
            });
            imagesThread.IsBackground = true;
            imagesThread.Start();
            //setting up everything for next element
            i++;
        }

        public void PlayImagesChromium(List<XmlElement> elementsToDisplay)//ne moze u helper clasu zbog UI elemenata
        {
            int timeout = 0;
            string fullNameOfElement;
            string elementName = "";
            string elementType = "";
            string mainElementOutput = "";
            for (int j = 0; j < elementsToDisplay.Count; j++)
            {
                //skip first element of every list because index = 0 is saved for info element
                if (elementsToDisplay[j].HasChildNodes)
                {
                    continue;
                }

                //j == 1 -  skip first child Element because it is required to be loaded to call PlayImagesChormium method, but letting him do his time
                if (j == 1)
                {
                    timeout = (int.Parse(elementsToDisplay[j].GetAttribute("timeout"))) * 1000;
                    Thread.Sleep(timeout);
                }

                else
                {
                    timeout = (int.Parse(elementsToDisplay[j].GetAttribute("timeout"))) * 1000;
                    fullNameOfElement = elementsToDisplay[j].GetAttribute("src");

                    var result = fullNameOfElement.Split('/');
                    var folderName = result[0];
                    if (elementsToDisplay[0].Name == "testcalibration")
                    {
                        folderName = "testcalibrationImages";
                    }
                    elementType = elementsToDisplay[0].Name;
                    mainElementOutput = elementsToDisplay[0].GetAttribute("output");
                    elementName = elementsToDisplay[j].GetAttribute("output");
                    synchronizationContext.Send((a) =>
                    {
                        chromiumBrowser.Address = $"file:///{WebTaskMediaDialog.generatedPagesPath}{elementType}-{mainElementOutput}-{elementName}.html";
                    }, null);
                    Thread.Sleep(timeout);
                }
            }
            synchronizationContext.Send((a) =>
            {
                var nextElementInOrder = WebTaskMediaDialog.GetNextXmlElement(allElementsInDefinition, ref i);
                DisplayToChromium(nextElementInOrder);

            }, null);
        }
        public void DisplayToChromium(XmlElement elementToDisplay)
        {
            var elementType = elementToDisplay.Name;
            var mainElementOutput = elementToDisplay.GetAttribute("output");
            var elementName = elementToDisplay.GetAttribute("info");

            var result = elementName.Split('/', '.');
            if (result.Length > 1)
            {
                elementName = result[1];
            }
            chromiumBrowser.Address = $"file:///{WebTaskMediaDialog.generatedPagesPath}{elementType}-{mainElementOutput}-{elementName}.html";
        }
    }
}
