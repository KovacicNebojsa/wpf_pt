﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PromoTester1.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("PromoTester1.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad video codec!.
        /// </summary>
        internal static string badCodecException {
            get {
                return ResourceManager.GetString("badCodecException", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conditions are not optimal! Please follow the guidelines and try again..
        /// </summary>
        internal static string badConditionsException {
            get {
                return ResourceManager.GetString("badConditionsException", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Camera disconnected!.
        /// </summary>
        internal static string cameraDisconnectedError {
            get {
                return ResourceManager.GetString("cameraDisconnectedError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Finishing web task, please wait....
        /// </summary>
        internal static string chromium_closing_text {
            get {
                return ResourceManager.GetString("chromium_closing_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Loading web task, please wait....
        /// </summary>
        internal static string chromium_load_text {
            get {
                return ResourceManager.GetString("chromium_load_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cleanup failed!.
        /// </summary>
        internal static string cleanupFailedError {
            get {
                return ResourceManager.GetString("cleanupFailedError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No internet connection! Could be that your internet connection was temporarily lost or your firewall is blocking the app to connect to the internet. Please check your firewall settings and allow EyeSee Test application to access the internet..
        /// </summary>
        internal static string connectionError {
            get {
                return ResourceManager.GetString("connectionError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can&apos;t create video!.
        /// </summary>
        internal static string createVideoException {
            get {
                return ResourceManager.GetString("createVideoException", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Directory error! .
        /// </summary>
        internal static string directory_error {
            get {
                return ResourceManager.GetString("directory_error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please don&apos;t move your head!.
        /// </summary>
        internal static string dontMoveYourHeadMessage {
            get {
                return ResourceManager.GetString("dontMoveYourHeadMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error.
        /// </summary>
        internal static string error_caption {
            get {
                return ResourceManager.GetString("error_caption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data uploaded successfully!.
        /// </summary>
        internal static string filesUploadedInfo {
            get {
                return ResourceManager.GetString("filesUploadedInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Eyetracking part done!.
        /// </summary>
        internal static string form1_info1 {
            get {
                return ResourceManager.GetString("form1_info1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Test interupted, please try again!.
        /// </summary>
        internal static string form1_info2 {
            get {
                return ResourceManager.GetString("form1_info2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, but test has expired..
        /// </summary>
        internal static string form1_info3 {
            get {
                return ResourceManager.GetString("form1_info3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login error!.
        /// </summary>
        internal static string form1_loginError {
            get {
                return ResourceManager.GetString("form1_loginError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This file is already done! Do you want to download and play it again?.
        /// </summary>
        internal static string form1_question {
            get {
                return ResourceManager.GetString("form1_question", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are about to close the EyeSee Test application. If you do so, you will not be able to complete the test.
        ///Are you sure you want to proceed with closing the application before you complete the test?.
        /// </summary>
        internal static string form1CloseWarning {
            get {
                return ResourceManager.GetString("form1CloseWarning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to FTP ListDir failed!.
        /// </summary>
        internal static string FtpListDirError {
            get {
                return ResourceManager.GetString("FtpListDirError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Information.
        /// </summary>
        internal static string info_caption {
            get {
                return ResourceManager.GetString("info_caption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Test empty!.
        /// </summary>
        internal static string infoDialogError1 {
            get {
                return ResourceManager.GetString("infoDialogError1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Internet Explorer not detected on your system! Please install Internet Explorer 11 to be able to see webpages properly..
        /// </summary>
        internal static string internetExplorerMissingWarning {
            get {
                return ResourceManager.GetString("internetExplorerMissingWarning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Internet Explorer detected on your system is outdated! Please install Internet Explorer 11 to be able to see webpages properly..
        /// </summary>
        internal static string internetExplorerOldVerWarning {
            get {
                return ResourceManager.GetString("internetExplorerOldVerWarning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Never.
        /// </summary>
        internal static string LastAccessDateNever {
            get {
                return ResourceManager.GetString("LastAccessDateNever", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Link Forbidden.
        /// </summary>
        internal static string linkForbidden_caption {
            get {
                return ResourceManager.GetString("linkForbidden_caption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This link is not active, please try another one....
        /// </summary>
        internal static string linkForbidden_text {
            get {
                return ResourceManager.GetString("linkForbidden_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log files uploaded!.
        /// </summary>
        internal static string logsUploadedInfo {
            get {
                return ResourceManager.GetString("logsUploadedInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No webcam device!.
        /// </summary>
        internal static string noWebcamError {
            get {
                return ResourceManager.GetString("noWebcamError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Choose video to play, please!.
        /// </summary>
        internal static string playRecordedVideo_warning {
            get {
                return ResourceManager.GetString("playRecordedVideo_warning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Analyzing data, please wait....
        /// </summary>
        internal static string progressBarDialog_analysing_label {
            get {
                return ResourceManager.GetString("progressBarDialog_analysing_label", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Calibrating....
        /// </summary>
        internal static string progressBarDialog_calibrating_label {
            get {
                return ResourceManager.GetString("progressBarDialog_calibrating_label", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Downloading and preparing test, please wait....
        /// </summary>
        internal static string progressBarDialog_downloading_label {
            get {
                return ResourceManager.GetString("progressBarDialog_downloading_label", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Finishing test, please wait....
        /// </summary>
        internal static string progressBarDialog_finishing_test_label {
            get {
                return ResourceManager.GetString("progressBarDialog_finishing_test_label", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Preparing next task, please wait....
        /// </summary>
        internal static string progressBarDialog_freebrowsing_upload_label {
            get {
                return ResourceManager.GetString("progressBarDialog_freebrowsing_upload_label", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Uploading data, please wait....
        /// </summary>
        internal static string progressBarDialog_uploading_label {
            get {
                return ResourceManager.GetString("progressBarDialog_uploading_label", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Uploading log files, please wait....
        /// </summary>
        internal static string progressBarDialog_uploading_log_label {
            get {
                return ResourceManager.GetString("progressBarDialog_uploading_log_label", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Question.
        /// </summary>
        internal static string question_caption {
            get {
                return ResourceManager.GetString("question_caption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Recording error!.
        /// </summary>
        internal static string recordingError {
            get {
                return ResourceManager.GetString("recordingError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can&apos;t write to sequence file!.
        /// </summary>
        internal static string sequenceFileError {
            get {
                return ResourceManager.GetString("sequenceFileError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Empty form!.
        /// </summary>
        internal static string surveyFormError {
            get {
                return ResourceManager.GetString("surveyFormError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No results obtained!.
        /// </summary>
        internal static string surveyResultsError {
            get {
                return ResourceManager.GetString("surveyResultsError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Tasks in taskgroup!.
        /// </summary>
        internal static string taskGroupNoTasksError {
            get {
                return ResourceManager.GetString("taskGroupNoTasksError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad calibration, please recalibrate!.
        /// </summary>
        internal static string testCalibrationCheckDialog_failure {
            get {
                return ResourceManager.GetString("testCalibrationCheckDialog_failure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Calibration successful, please continue!.
        /// </summary>
        internal static string testCalibrationCheckDialog_success {
            get {
                return ResourceManager.GetString("testCalibrationCheckDialog_success", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Test not available, maximum attempts count reached! Please contact us!.
        /// </summary>
        internal static string testDisabledInfo {
            get {
                return ResourceManager.GetString("testDisabledInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are about to terminate the test. Are you sure?.
        /// </summary>
        internal static string testTerminationInfo {
            get {
                return ResourceManager.GetString("testTerminationInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connection Error!.
        /// </summary>
        internal static string universalTestReader_connectionError {
            get {
                return ResourceManager.GetString("universalTestReader_connectionError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login failed! Please try again!.
        /// </summary>
        internal static string universalTestReader_loginFailed {
            get {
                return ResourceManager.GetString("universalTestReader_loginFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter username and password!.
        /// </summary>
        internal static string userPassInfo {
            get {
                return ResourceManager.GetString("userPassInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad entry parameter! Max 4 paths!.
        /// </summary>
        internal static string videoCollectionMediaDialog_constructorError {
            get {
                return ResourceManager.GetString("videoCollectionMediaDialog_constructorError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Warning.
        /// </summary>
        internal static string warning_caption {
            get {
                return ResourceManager.GetString("warning_caption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Closing....
        /// </summary>
        internal static string webCamCheckDialog_closingButtonText {
            get {
                return ResourceManager.GetString("webCamCheckDialog_closingButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad camera resolution or camera video not loaded!
        ///Please check if your web camera is working properly and set your web camera resolution to 640x480.
        ///You can test your camera by selecting Options &gt; Test Webcam in the menu bar.
        ///If your camera is not working, check if it is plugged in properly and close other applications that might be using it. If this does not solve this issue, please reset your computer or reinstall the camera driver..
        /// </summary>
        internal static string webCamException {
            get {
                return ResourceManager.GetString("webCamException", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Low camera FPS Rate! Please turn off all other applications and try again.
        ///Please check if your web camera is working properly and set your web camera resolution to 640x480.
        ///You can test your camera by selecting Options &gt; Test Webcam in the menu bar.
        ///If your camera is not working, check if it is plugged in properly and close other applications that might be using it. If this does not solve this issue, please reset your computer or reinstall the camera driver..
        /// </summary>
        internal static string webcamReader_fpsWarning {
            get {
                return ResourceManager.GetString("webcamReader_fpsWarning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Camera not detected!
        ///Please check if your web camera is working properly and set your web camera resolution to 640x480.
        ///You can test your camera by selecting Options &gt; Test Webcam in the menu bar.
        ///If your camera is not working, check if it is plugged in properly and close other applications that might be using it. If this does not solve this issue, please reset your computer or reinstall the camera driver..
        /// </summary>
        internal static string webcamReader_noCameraWarning {
            get {
                return ResourceManager.GetString("webcamReader_noCameraWarning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RecordingError!.
        /// </summary>
        internal static string webCamReader_recordingError {
            get {
                return ResourceManager.GetString("webCamReader_recordingError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad camera resolution!
        ///Please check if your web camera is working properly and set your web camera resolution to 640x480.
        ///You can test your camera by selecting Options &gt; Test Webcam in the menu bar.
        ///If your camera is not working, check if it is plugged in properly and close other applications that might be using it. If this does not solve this issue, please reset your computer or reinstall the camera driver..
        /// </summary>
        internal static string webcamReader_resolutionWarning {
            get {
                return ResourceManager.GetString("webcamReader_resolutionWarning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Web camera streaming error!.
        /// </summary>
        internal static string webcamReader_streamingError {
            get {
                return ResourceManager.GetString("webcamReader_streamingError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can&apos;t save webCamShot image.
        /// </summary>
        internal static string webCamShotSaveError {
            get {
                return ResourceManager.GetString("webCamShotSaveError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome to EyeSee Test!
        ///To begin, select a test from the list, double click on it and follow the wizard instructions. Wishing you a pleasant experience!.
        /// </summary>
        internal static string welcomeDialogText {
            get {
                return ResourceManager.GetString("welcomeDialogText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check attributes of &lt;calibration&gt; elements!.
        /// </summary>
        internal static string xmlException_calib_attr {
            get {
                return ResourceManager.GetString("xmlException_calib_attr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check &lt;calibration&gt; element names!.
        /// </summary>
        internal static string xmlException_calib_elem {
            get {
                return ResourceManager.GetString("xmlException_calib_elem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check &lt;calibration&gt; tags!.
        /// </summary>
        internal static string xmlException_calibration {
            get {
                return ResourceManager.GetString("xmlException_calibration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check &lt;clicktask&gt; tags!.
        /// </summary>
        internal static string xmlException_click {
            get {
                return ResourceManager.GetString("xmlException_click", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check attributes of &lt;clicktask&gt; elements!.
        /// </summary>
        internal static string xmlException_click_attr {
            get {
                return ResourceManager.GetString("xmlException_click_attr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, calibration and clicktask have different onlineAnalysis attribute!.
        /// </summary>
        internal static string xmlException_click_calib {
            get {
                return ResourceManager.GetString("xmlException_click_calib", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check &lt;clicktask&gt; tags!.
        /// </summary>
        internal static string xmlException_click_elem {
            get {
                return ResourceManager.GetString("xmlException_click_elem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check element names!.
        /// </summary>
        internal static string xmlException_elem {
            get {
                return ResourceManager.GetString("xmlException_elem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check &lt;info&gt; tags!.
        /// </summary>
        internal static string xmlException_info {
            get {
                return ResourceManager.GetString("xmlException_info", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check &lt;testcalibration&gt; tags!.
        /// </summary>
        internal static string xmlException_testCalib {
            get {
                return ResourceManager.GetString("xmlException_testCalib", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, calibration and testcalibration have different onlineAnalysis attribute!.
        /// </summary>
        internal static string xmlException_testCalib_calib {
            get {
                return ResourceManager.GetString("xmlException_testCalib_calib", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check &lt;videotask&gt; tags!.
        /// </summary>
        internal static string xmlException_video {
            get {
                return ResourceManager.GetString("xmlException_video", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check attributes of &lt;videotask&gt; elements!.
        /// </summary>
        internal static string xmlException_video_attr {
            get {
                return ResourceManager.GetString("xmlException_video_attr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, calibration and videotask have different onlineAnalysis attribute!.
        /// </summary>
        internal static string xmlException_video_calib {
            get {
                return ResourceManager.GetString("xmlException_video_calib", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check &lt;videotask&gt; tags!.
        /// </summary>
        internal static string xmlException_video_elem {
            get {
                return ResourceManager.GetString("xmlException_video_elem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check &lt;watchtask&gt; tags!.
        /// </summary>
        internal static string xmlException_watch {
            get {
                return ResourceManager.GetString("xmlException_watch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check attributes of &lt;watchtask&gt; elements!.
        /// </summary>
        internal static string xmlException_watch_attr {
            get {
                return ResourceManager.GetString("xmlException_watch_attr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, calibration and watchtask have different onlineAnalysis attribute!.
        /// </summary>
        internal static string xmlException_watch_calib {
            get {
                return ResourceManager.GetString("xmlException_watch_calib", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check &lt;watchtask&gt; tags!.
        /// </summary>
        internal static string xmlException_watch_elem {
            get {
                return ResourceManager.GetString("xmlException_watch_elem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check &lt;webtask&gt; tags!.
        /// </summary>
        internal static string xmlException_web {
            get {
                return ResourceManager.GetString("xmlException_web", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check attributes of &lt;webtask&gt; elements!.
        /// </summary>
        internal static string xmlException_web_attr {
            get {
                return ResourceManager.GetString("xmlException_web_attr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, calibration and webtask have different onlineAnalysis attribute!.
        /// </summary>
        internal static string xmlException_web_calib {
            get {
                return ResourceManager.GetString("xmlException_web_calib", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad XML file, check &lt;webtask&gt; tags!.
        /// </summary>
        internal static string xmlException_web_elem {
            get {
                return ResourceManager.GetString("xmlException_web_elem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Check zip file path!.
        /// </summary>
        internal static string zipFileError {
            get {
                return ResourceManager.GetString("zipFileError", resourceCulture);
            }
        }
    }
}
