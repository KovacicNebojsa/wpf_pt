'use strict';

console.log('Cao, Nebojsa Ovdje');

const btnPrev = document.querySelector('.btn-left');
const btnNext = document.querySelector('.btn-right');

btnPrev.addEventListener('mouseover', function () {
  if (
    window.location.pathname.includes('intro02') ||
    window.location.pathname.includes('intro05') ||
    window.location.pathname.includes('intro06') 
  ) {
    btnPrev.disabled = false;
  } else {
    btnPrev.disabled = true;
  }
});
