﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromoTester1
{
    class PtWebBrowser
    {
        public UniversalBrowser browser;

        public Uri url;
        //public List<WebTaskSticker> webTaskStickers;
        public int timeout;
        public String stimuli_output;
        public int stimuli_id;
        public bool finalTarget;
        public bool docLoaded;

        public int helpMarkPositionX;
        public int helpMarkPositionY;
        public System.Drawing.Point helpMarkScrollPosition;
        public int helpTimeout;
        public String helpMessage;
        public int helpWidth;
        public int helpHeight;
        public int helpStartPositionX;
        public int helpStartPositionY;

        public bool manualNavigation;

        public PtWebBrowser(String address, bool useChromium)
        {
            docLoaded = false;

            timeout = 0;
            stimuli_output = "";
            stimuli_id = -1;
            finalTarget = false;
            //webTaskStickers = null;

            helpMarkPositionX = 0;
            helpMarkPositionY = 0;
            helpMarkScrollPosition = new System.Drawing.Point(0, 0);
            helpTimeout = 0;
            helpMessage = "";
            helpWidth = 0;
            helpHeight = 0;
            helpStartPositionX = 0;
            helpStartPositionY = 0;

            url = null;
            browser = null;

            if (address != null)
            {
                browser = new UniversalBrowser(address);
            }
        }

        public void setAllButBrowser(PtWebBrowser ptwb)
        {
            docLoaded = ptwb.docLoaded;

            timeout = ptwb.timeout;
            stimuli_output = ptwb.stimuli_output;
            stimuli_id = ptwb.stimuli_id;
            finalTarget = ptwb.finalTarget;
            //webTaskStickers = ptwb.webTaskStickers;

            helpMarkPositionX = ptwb.helpMarkPositionX;
            helpMarkPositionY = ptwb.helpMarkPositionY;
            helpMarkScrollPosition = ptwb.helpMarkScrollPosition;
            helpTimeout = ptwb.helpTimeout;
            helpMessage = ptwb.helpMessage;
            helpHeight = ptwb.helpHeight;
            helpWidth = ptwb.helpWidth;
            helpStartPositionX = ptwb.helpStartPositionX;
            helpStartPositionY = ptwb.helpStartPositionY;

            url = ptwb.url;
        }
    }
}

