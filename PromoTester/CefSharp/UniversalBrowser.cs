﻿using CefSharp;
using CefSharp.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromoTester1
{
    public class UniversalBrowser : IJsDialogHandler
    {
        private object browser;
        private ChromiumWebBrowser chromiumBrowser
        {
            get { return (ChromiumWebBrowser)browser; }
        }
        public bool CanGoBack
        {
            get
            {
                return chromiumBrowser.CanGoBack;
            }
        }
        public UniversalBrowser(string address)
        {
            if (address != null)
            {
                browser = new ChromiumWebBrowser(address)
                {
                    JsDialogHandler = this,
                };

                chromiumBrowser.JsDialogHandler = this;
                chromiumBrowser.LifeSpanHandler = new LifeSpanHandler();
                CefSharpSettings.LegacyJavascriptBindingEnabled = true;
            }
        }
        public void Back()
        {
            chromiumBrowser.Back();
        }

        public void Load(string href)
        {
           chromiumBrowser.Load(href);
        }

        public bool OnJSDialog(IWebBrowser chromiumWebBrowser, IBrowser browser, string originUrl, CefJsDialogType dialogType, string messageText, string defaultPromptText, IJsDialogCallback callback, ref bool suppressMessage)
        {
            throw new NotImplementedException();
        }

        public bool OnBeforeUnloadDialog(IWebBrowser chromiumWebBrowser, IBrowser browser, string messageText, bool isReload, IJsDialogCallback callback)
        {
            throw new NotImplementedException();
        }

        public void OnResetDialogState(IWebBrowser chromiumWebBrowser, IBrowser browser)
        {
            throw new NotImplementedException();
        }

        public void OnDialogClosed(IWebBrowser chromiumWebBrowser, IBrowser browser)
        {
            throw new NotImplementedException();
        }
    }
}
