﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PromoTester1
{
    public static class TesterAlgo
    {
        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "Initialize", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Initialize(String logPath, int screenW, int screenH, int camW, int camH, double imageQualityTh, int headMovementTh, double headZoomTh, double headRotationTh, double calibDistTh);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "SetParams", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SetParams(String iniPath);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "Connect", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Connect(String dataFolderPath, String auth_key, IntPtr imgPtr, int operationMode, int log_data, ref double imgQ);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "ConnectEmoCheck", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ConnectEmoCheck(String dataFolderPath, String auth_key, IntPtr imgPtr, int operationMode, int log_data, ref double imgQ);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "analyzeImage", CallingConvention = CallingConvention.Cdecl)]
        public static extern int analyzeImage(IntPtr imgPtr, ref int x, ref int y, ref int x_corrected, ref int y_corrected, int log_data, ref double imgQ, ref double head_mov, ref double face_rel_size);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "analyzeImageEmo", CallingConvention = CallingConvention.Cdecl)]
        public static extern int analyzeImageEmo(IntPtr imgPtr, int _do_eye_gaze, int _do_emotions, int _do_heatMap, ref int x, ref int y, ref int x_corrected, ref int y_corrected, int log_data, ref double imgQ, ref double head_mov, ref double face_rel_size, ref double emo_neutral, ref double emo_happy, ref double emo_supr, ref double emo_angry, ref double emo_disg, ref double emo_afraid, ref double emo_sad);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "emoCheckEmotions", CallingConvention = CallingConvention.Cdecl)]
        public static extern int emoCheckEmotions(IntPtr imgPtr, int log_data, ref double imgQ, ref double emo_neutral, ref double emo_happy, ref double emo_supr, ref double emo_angry, ref double emo_disg, ref double emo_afraid, ref double emo_sad);


        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "getHeatMap", CallingConvention = CallingConvention.Cdecl)]
        public static extern int getHeatMap(ref byte data, ref int x, ref int y);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "resetHeatMap", CallingConvention = CallingConvention.Cdecl)]
        public static extern int resetHeatMap();

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "analyzeImageScroll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int analyzeImageScroll(IntPtr imgPtr, ref int x, ref int y, ref int x_corrected, ref int y_corrected, int w, int h, int delta_x, int delta_y, int log_data, ref double imgQ, ref double head_mov, ref double face_rel_size);


        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "analyzeImageScrollEmo", CallingConvention = CallingConvention.Cdecl)]
        public static extern int analyzeImageScrollEmo(IntPtr imgPtr, int _do_eye_gaze, int _do_emotions, int _do_heatMap, ref int x, ref int y, ref int x_corrected, ref int y_corrected, int w, int h, int delta_x, int delta_y, int log_data, ref double imgQ, ref double head_mov, ref double face_rel_size, ref double emo_neutral, ref double emo_happy, ref double emo_supr, ref double emo_angry, ref double emo_disg, ref double emo_afraid, ref double emo_sad);


        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "getHeatMapScroll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int getHeatMapScroll(ref byte data);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "resetHeatMapScroll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int resetHeatMapScroll();

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "addCalibrationPoint", CallingConvention = CallingConvention.Cdecl)]
        public static extern int addCalibrationPoint(IntPtr imgPtr, int calibPointX, int calibPointY, int frameNo, int isMiddleThird, int calibType, int log_data, ref double imgQ, ref double head_mov, ref double face_rel_size);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "doCalibrate", CallingConvention = CallingConvention.Cdecl)]
        public static extern int doCalibrate(int calibType, int log_data, ref int no_RE_outliers, ref double avg_RE);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "Cleanup", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Cleanup();

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "CleanupLiveStream", CallingConvention = CallingConvention.Cdecl)]
        public static extern int CleanupLiveStream();

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "CleanupEmoCheck", CallingConvention = CallingConvention.Cdecl)]
        public static extern int CleanupEmoCheck();


        //[DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "testCalibration", CallingConvention = CallingConvention.StdCall)]
        //public static extern int testCalibration(IntPtr imgPtr, ref int out_x, ref int out_y, ref int out_x_corrected, ref int out_y_corrected, int in_x, int in_y, int log_data, ref double imgQ, ref double out_head_mov, ref double out_face_rel_size);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "testCalibration", CallingConvention = CallingConvention.Cdecl)]
        public static extern int testCalibration(IntPtr imgPtr, int in_x, int in_y, int log_data, ref double imgQ, ref double out_head_mov, ref double out_face_rel_size, ref double out_Error);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "serializeCalibration", CallingConvention = CallingConvention.Cdecl)]
        public static extern int serializeCalibration(String calibInfoPath);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "deserializeCalibration", CallingConvention = CallingConvention.Cdecl)]
        public static extern int deserializeCalibration(String calibInfoPath);

        //Displays red and green rect inside camera image that displays is you positioned well
        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "getFaceRectQuality", CallingConvention = CallingConvention.Cdecl)]
        public static extern int getFaceRectQuality(IntPtr imgPtr, ref int error1, ref int error2, ref int error3, ref int error4, ref int error5, ref int error6);//, String dataFolderPath);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "InitializeTestFace", CallingConvention = CallingConvention.Cdecl)]
        public static extern void InitializeTestFace(String logPath, int camW, int camH, String dataFolderPath, double min_quality_live);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "InitializeEmoCheck", CallingConvention = CallingConvention.Cdecl)]
        public static extern void InitializeEmoCheck(String logPath, int camW, int camH);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "addHomographyPoints", CallingConvention = CallingConvention.Cdecl)]
        public static extern int addHomographyPoints(int xx, int yy, int calibX, int calibY);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "findHomography", CallingConvention = CallingConvention.Cdecl)]
        public static extern int findHomography();

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "returnCorrectedHomographyPoints", CallingConvention = CallingConvention.Cdecl)]
        public static extern int returnCorrectedHomographyPoints(int index, ref int x, ref int y);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "CleanupEndTest", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CleanupEndTest();

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "logHomographyMatrix", CallingConvention = CallingConvention.Cdecl)]
        public static extern int logHomographyMatrix(String logPath, String logSource);

        [DllImport("opencvTmp\\PromoTesterAlgo.dll", EntryPoint = "addPointToHeatMap", CallingConvention = CallingConvention.Cdecl)]
        public static extern int addPointToHeatMap(int xx, int yy);
    }
}
